
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.io.DataOutput;
import java.io.DataInput;

import org.apache.hadoop.conf.Configuration;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;



public class MapReduce
{

	public static class VMapper extends Mapper<Object, Text, VCityIDWritable, IntWritable>
	{
		private final static IntWritable one = new IntWritable( 1 );
		//private static final Log log = LogFactory.getLog(VMapper.class);
		
		public void map( Object key, Text value, Context context ) throws IOException, InterruptedException, NumberFormatException 
		{
			//log.info("AZAZAZAZ");
			
			String[] par = value.toString().split("\\s+");

			int len = par.length;
			int price;
			int city_id;
			
			if( len < 17 )
				return;
			
			try
			{
				price = Integer.parseInt( par[len-1-3] );
				city_id = Integer.parseInt( par[len-1-16] );
			}
			catch(NumberFormatException e)
			{
				return;
			}
			
			if( price > 250 )
			{
				context.write( new VCityIDWritable( city_id ), one );
			}
		}
		
	}

	

	public static class VCombiner extends Reducer<VCityIDWritable, IntWritable, VCityIDWritable, IntWritable>
	{
		private IntWritable result = new IntWritable();

		public void reduce( VCityIDWritable key, Iterable<IntWritable> values, Context context ) throws IOException, InterruptedException
		{
			int sum = 0;
			
			for( IntWritable val : values )
			{
				sum += val.get();
			}
			
			result.set( sum );
			
			context.write( key , result );
		}		
	}
	
	
	
	public static class VReducer extends Reducer<VCityIDWritable, IntWritable, Text, IntWritable>
	{
		private IntWritable result = new IntWritable();
		private HashMap<Integer, String> city = new HashMap<Integer, String>();
		
		
		public void setup( Context context ) throws IOException, InterruptedException
		{
			city.put( 0, "unknown" );
			readCity( "(C)", "hdfs://sandbox.hortonworks.com:8020/map_reduce/input/city.en.txt", context );
			readCity( "(R)", "hdfs://sandbox.hortonworks.com:8020/map_reduce/input/region.en.txt", context );
		}
		
		
		private void readCity( String pref, String p, Context context ) throws IOException, InterruptedException
		{
			Path path = new Path( p );
			FileSystem fs = FileSystem.get( context.getConfiguration() );
			BufferedReader br = new BufferedReader( new InputStreamReader( fs.open(path) ) );
			
			try
			{
				String line;
				line=br.readLine();
				while ( line != null )
				{
					String[] par = line.split("\\s+");
					int id = Integer.parseInt( par[0] );
					
					if( !city.containsKey( id ) )
						city.put( id, pref+par[1] );					
					
					line = br.readLine();
				}
			}
			finally
			{
				br.close();
			}				
		}
				

		public void reduce( VCityIDWritable key, Iterable<IntWritable> values, Context context ) throws IOException, InterruptedException
		{
			int sum = 0;
			
			for( IntWritable val : values )
			{
				sum += val.get();
			}
			
			result.set( sum );
			
			if( city.containsKey( key.get() ) )
				context.write( new Text( city.get( key.get() ) ) , result );
			else
				context.write( new Text( "noname["+key.toString()+"]" ) , result );
		}
	}
	
	

	public static void main( String[] args ) throws Exception
	{
		Configuration conf = new Configuration();

		Job job = Job.getInstance( conf, "map reduce" );
		
		job.setJarByClass( MapReduce.class );
		job.setMapperClass( VMapper.class );
		job.setCombinerClass( VCombiner.class );
		job.setReducerClass( VReducer.class );
		
		job.setMapOutputKeyClass( VCityIDWritable.class );
		job.setMapOutputValueClass( IntWritable.class );	
		job.setOutputKeyClass( Text.class );
		job.setOutputValueClass( IntWritable.class );
		
		job.setOutputFormatClass( SequenceFileOutputFormat.class );

		FileInputFormat.addInputPath( job, new Path(args[0]) );
		//FileOutputFormat.setOutputPath( job, new Path(args[1]) );
		
		SequenceFileOutputFormat.setOutputPath( job, new Path(args[1]) );
		SequenceFileOutputFormat.setCompressOutput( job, true );
		SequenceFileOutputFormat.setOutputCompressorClass( job, SnappyCodec.class );
		//SequenceFileOutputFormat.setOutputCompressionType( job, CompressionType.BLOCK );
		
		System.exit( job.waitForCompletion(true) ? 0 : 1 );
	}
  
  
  
}








