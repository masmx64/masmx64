
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.Text;

import org.junit.Before;
import org.junit.Test;
import java.io.IOException;


public class VMapperTest
{

    MapDriver<Object, Text, VCityIDWritable, IntWritable> mapDriver;

    @Before
    public void setUp()
	{
        MapReduce.VMapper mapper = new MapReduce.VMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void testMapperNormal() throws IOException
	{
		mapDriver.withInput(new LongWritable(), new Text("50c5868563692abf62858d9b92f2fa9b	20131024163900589	1	D15G9j9Rffr " +
			"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1	218.24.31.*	40 " +
			"44	1	d7c42b08410f0d9e986d9884311d2f2d	d3cfb0c83b8de2b93a9476edce0bef18	null	mm_31464994_2990028_11198855 " +
			"300	250	Na	Na	0	12622	294	294	null	2261	16593,14273,10076,10093,13042,10129,10102,10006,10024,10110,13776," +
			"10031,10120,10052,10145,13403,10115\n"));
			
		mapDriver.withInput(new LongWritable(), new Text("5dd250f9a46ab7a607f4809bd5004a27	20131024172100419	1	D4UHkIDWwlx	Mozilla/4.0 "+
			" (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; SE 2.X MetaSr 1.0)	1.61.7.*	65	77	2	"+
			"aedc85a495b5fc0aaa4f5ac9254b7372	897327a8776948f175a007a9bd4b8806	null	2074793036	336	280	OtherView	Na	180	12623	"+
			"180	180	null	2261	10057,10059,14273,10129,10102,10024,10006,10110,13776,10120,10145,10115,10063\n"));				
		
		mapDriver.withInput(new LongWritable(), new Text("231eeb0fa4ec64653fc995cee184f987	20131024175200387	1	D5NGY3BCGrM	Mozilla/5.0"+
			" (Windows NT 6.1; rv:22.0) Gecko/20100101 Firefox/22.0	112.192.177.*	276	286	1	1651a4221030581e210374e626068fd6"+
			"	7c80a8627deb6cc9ad543fd087a64f61	null	mm_30096303_2982811_11477818	300	251	Na	Na	0	12622	294	270	null"+
			"	2261	10048,10059,10102,10006,10024,13776,10146,10111,11944,10115,16753,10063\n"));				
		
		mapDriver.withOutput(new VCityIDWritable(44), new IntWritable(1));
		mapDriver.withOutput(new VCityIDWritable(286), new IntWritable(1));
		
		mapDriver.runTest();		
    }
	
    @Test
    public void testMapperIncorrectCID() throws IOException
	{
		mapDriver.withInput(new LongWritable(), new Text("50c5868563692abf62858d9b92f2fa9b	20131024163900589	1	D15G9j9Rffr " +
			"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1	218.24.31.*	40 " +
			"XXX	1	d7c42b08410f0d9e986d9884311d2f2d	d3cfb0c83b8de2b93a9476edce0bef18	null	mm_31464994_2990028_11198855 " +
			"250	250	Na	Na	0	12622	294	294	null	2261	16593,14273,10076,10093,13042,10129,10102,10006,10024,10110,13776," +
			"10031,10120,10052,10145,13403,10115\n"));
			
		mapDriver.runTest();
    }
	
    @Test
    public void testMapperIncorrectBID() throws IOException
	{
		mapDriver.withInput(new LongWritable(), new Text("50c5868563692abf62858d9b92f2fa9b	20131024163900589	1	D15G9j9Rffr " +
			"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1	218.24.31.*	40 " +
			"40	1	d7c42b08410f0d9e986d9884311d2f2d	d3cfb0c83b8de2b93a9476edce0bef18	null	mm_31464994_2990028_11198855 " +
			"250	250	Na	Na	0	12622	XXX 294	null	2261	16593,14273,10076,10093,13042,10129,10102,10006,10024,10110,13776," +
			"10031,10120,10052,10145,13403,10115\n"));
			
		mapDriver.runTest();
    }	
	
    @Test
    public void testMapperIncorrectBadLine() throws IOException
	{
		mapDriver.withInput(new LongWritable(), new Text("AZAZAZA XXX YYY\n"));
			
		mapDriver.runTest();
    }	
}