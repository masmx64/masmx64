
import java.io.IOException;
import java.io.DataOutput;
import java.io.DataInput;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.WritableComparable;



public class VCityIDWritable implements WritableComparable<VCityIDWritable>
{
	private int id;
	
	public VCityIDWritable() {}
	
	public VCityIDWritable(int a) { set(a); }
	
	public void write(DataOutput out) throws IOException
	{
		out.writeInt( id );
	}
	
	public void readFields(DataInput in) throws IOException
	{
		id = in.readInt();
	}

	public int compareTo(VCityIDWritable a)
	{
		if(id < a.id)
			return -1;
		
		if(id > a.id)
			return 1;
		
		return 0;
	}
	
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof VCityIDWritable)) {
			return false;
		}
		VCityIDWritable other = (VCityIDWritable) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}	
	
	public String toString()
	{
		return String.valueOf(id);
	}
	
	public int hashCode()
	{
		return id;
	}		
	
	public void set(int a)
	{
		id = a;
	}
	
	public int get()
	{
		return id;
	}			
}