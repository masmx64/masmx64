#!/bin/bash

javac -classpath $(hadoop classpath) -d ./classes MapReduce.java VCityIDWritable.java

if [ "$?" -ne "0" ];
then
	echo "<<<COMPILE ERROR>>>"
	exit
fi


jar -cvf MapReduce.jar -C ./classes .

if [ "$?" -ne "0" ];
then
	echo "<<<JAR ERROR>>>"
	exit
fi


echo "[SUCESS]"
